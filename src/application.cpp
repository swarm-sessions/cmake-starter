#include "application.hpp"


Application::Application(std::string message)
{
    this->message = message;
}
    
Application::execute(void)
{
    std::cout << "Application started ..." << std::endl;
    std::cout << message << std::endl;
    std::cout << "Application finished !" << std::endl;
}


int main(int argc, char *argv[]){
   Application a("Hallo World!");
   a.execute();
   return 0;
}