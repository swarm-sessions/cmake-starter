#include<string>


class Application
{
    
public:
    Application(std::string message);
    void execute(void);

private:
    std::string message;

};